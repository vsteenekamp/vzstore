# Store
This is a basic Java EE application that persists a customer object to a database.

## Data source configuration
The store application is configured to use

    <jta-data-source>java:/StoreDS</jta-data-source>

---
store/store-ejb/src/main/resources/META-INF/persistence.xml
 
    <?xml version="1.0" encoding="UTF-8"?>
    <persistence version="2.1"
    	xmlns="http://xmlns.jcp.org/xml/ns/persistence"
    	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    	xsi:schemaLocation="
            http://xmlns.jcp.org/xml/ns/persistence
            http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd">
    	<persistence-unit name="primary"
    		transaction-type="JTA">
    		<jta-data-source>java:/StoreDS</jta-data-source>
    
    		<properties>
    			<property
    				name="javax.persistence.schema-generation.database.action"
    				value="drop-and-create"/>
    		</properties>
    	</persistence-unit>
    </persistence>

---
Wildfly data source configuration:
(/wildfly-13.0.0.Final/standalone/configuration/standalone.xml)

                <datasource jndi-name="java:/StoreDS" pool-name="StoreDS">
                    <connection-url>jdbc:postgresql://localhost:5433/storedb</connection-url>
                    <driver-class>org.postgresql.Driver</driver-class>
                    <driver>postgresql-42.2.5.jre7.jar</driver>
                    <security>
                        <user-name>storeadmin</user-name>
                        <password>xxx</password>
                    </security>
                    <validation>
                        <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLValidConnectionChecker"/>
                        <background-validation>true</background-validation>
                        <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLExceptionSorter"/>
                    </validation>
                </datasource>

