package za.co.vzs.store.persistence;


import java.util.List;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import za.co.vzs.store.domain.Audit;

@Dependent
public class AuditResourceImpl implements AuditResource {
	
	@PersistenceContext
	private EntityManager em;

    public AuditResourceImpl() {        
    }
    
    
	@Override
	public void create(Audit audit) {
		 em.persist(audit);		
	}

    
	@Override
	public Audit find(int id){
		return em.find(Audit.class, id);
	}

    
	@Override
	public List<Audit> findAll() {
		CriteriaQuery<Audit> cq = em.getCriteriaBuilder().createQuery(Audit.class);        
        cq.select(cq.from(Audit.class));
        return em.createQuery(cq).getResultList();
	}

}
