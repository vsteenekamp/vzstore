package za.co.vzs.store.persistence;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import za.co.vzs.store.domain.Customer;

@Dependent
public class CustomerResourceImpl implements CustomerResource {

	@PersistenceContext
	private EntityManager em;

	
	public CustomerResourceImpl() {		
	}
		
	
	@Override
	public void create(Customer customer) {		
		em.persist(customer);		
	}

	
	@Override
	public void edit(Customer customer) {
		em.merge(customer);		
	}
	
	@Override
	public void remove(Customer customer) {
		em.remove(customer);
		
		
	}

	@Override
	public Customer find(int id) {
		return em.find(Customer.class, id);
	}

	@Override
	public List<Customer> findAll() {
        CriteriaQuery<Customer> cq = em.getCriteriaBuilder().createQuery(Customer.class);        
        cq.select(cq.from(Customer.class));
        return em.createQuery(cq).getResultList();
	}
	
	@Override
	public boolean emailExist(String email) {		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Customer> customer = cq.from(Customer.class);		
		cq.select(cb.count(customer));
		cq.where(cb.equal(customer.get("email"), email));
		return em.createQuery(cq).getSingleResult() > 0;
	}

}
