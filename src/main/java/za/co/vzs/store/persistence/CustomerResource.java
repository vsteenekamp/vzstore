package za.co.vzs.store.persistence;

import java.util.List;

import za.co.vzs.store.domain.Customer;

public interface CustomerResource {

	public void create(Customer entity);

	public void edit(Customer entity);

	public void remove(Customer entity);

	public Customer find(int id);

	public List<Customer> findAll();	

	public boolean emailExist(String email);

}