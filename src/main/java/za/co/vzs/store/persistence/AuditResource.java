package za.co.vzs.store.persistence;

import java.util.List;

import za.co.vzs.store.domain.Audit;

public interface AuditResource {

	public void create(Audit entity);

	public Audit find(int id);

	public List<Audit> findAll();	

}