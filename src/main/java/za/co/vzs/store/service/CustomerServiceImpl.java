package za.co.vzs.store.service;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import za.co.vzs.store.domain.Audit;
import za.co.vzs.store.domain.Customer;
import za.co.vzs.store.persistence.AuditResource;
import za.co.vzs.store.persistence.CustomerResource;

@Stateless
public class CustomerServiceImpl implements CustomerService {

	@Inject
	private CustomerResource customerResource;
	@Inject
	private AuditResource auditResource;

	public CustomerServiceImpl() {
	}

	@Override
	public void addCustomer(Customer customer) {
		if (customerResource.emailExist(customer.getEmail())) {
			throw new ValidationException("VALIDATE_ERROR_001");
		}		
		customerResource.create(customer);
		createAudit(customer, Audit.UpdateType.CREATE);
	}

	@Override
	public List<Customer> getCustomers() {
		return customerResource.findAll();
	}

	private void createAudit(Customer customer, Audit.UpdateType type) {
		Audit audit = new Audit();
		audit.setDate(Calendar.getInstance());
		audit.setEntityId(customer.getId());
		audit.setType(type);
		auditResource.create(audit);
	}

}
