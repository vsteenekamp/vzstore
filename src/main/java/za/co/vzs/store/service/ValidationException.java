package za.co.vzs.store.service;

import javax.ejb.ApplicationException;

/**
 * This exception is thrown when validation of a Customer entity fails.
 * 
 */
@ApplicationException
public class ValidationException extends RuntimeException {

	private static final long serialVersionUID = 1279027750257713447L;
	
	private String errorCode;

	/**
	 * Constructs CustomerValidationException.
	 * 	  
	 * @param errorCode describing why validation failed. 
	 */
	public ValidationException(String errorCode) {
		super();
		this.errorCode = errorCode;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

}
