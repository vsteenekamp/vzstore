package za.co.vzs.store.service;

import java.util.List;

import javax.ejb.Local;

import za.co.vzs.store.domain.Customer;

/**
 * This declares a local business interface used to add and retrieve
 * Customer entities.
 *    
 */

@Local
public interface CustomerService { 
	
	/**
	 * Adds new customer to data store.
	 * @param customer the customer entity to be added.
	 * @throws ValidationException if a validation error occurs.
	 */
	public void addCustomer(Customer customer) throws ValidationException;
	
	/**
	 * Return is list of customers in the data store.
	 * @return a List of Customers.
	 */
	public List<Customer> getCustomers();
		 
}
	