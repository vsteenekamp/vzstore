package za.co.vzs.store.web;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.log4j.Logger;

import za.co.vzs.store.domain.Address;
import za.co.vzs.store.domain.Customer;
import za.co.vzs.store.service.CustomerService;
import za.co.vzs.store.service.ValidationException;

@Named("customerBean")
@RequestScoped
public class CustomerBean {

	final static Logger logger = Logger.getLogger(CustomerBean.class);

	public CustomerBean() {
	}

    @EJB	
	private CustomerService customerService;

	private String firstName;
	private String lastName;
	private String addressFirstLine;
	private String postalCode;
	private String email;

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String name) {
		this.firstName = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddressFirstLine() {
		return this.addressFirstLine;
	}

	public void setAddressFirstLine(String firstLine) {
		this.addressFirstLine = firstLine;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String contact) {
		this.email = contact;
	}

	public String submit() {
		String nextPage = "";
		Customer customer = new Customer();
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setEmail(email);
		Address billingAddress = new Address();
		billingAddress.setFirstLine(addressFirstLine);
		billingAddress.setPostalCode(postalCode);
		customer.setBillingAddress(billingAddress);
		try {
			customerService.addCustomer(customer);
			nextPage = "add-success?faces-redirect=true&amp;includeViewParams=true";
		} catch (ValidationException e) {
			addMessage(getMessage(e.getErrorCode()));
		} catch (EJBException ex) {
			logger.error("Failed to add customer.", ex);			
			addMessage(ResourceBundle.getBundle("customerbundle").getString("CustomerSubmitFailed"));
		}
		return nextPage;
	}

	private void addMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(message));
	}

	private String getMessage(String errorCode) {
		String errorMessage;
		try {
			errorMessage = ResourceBundle.getBundle("errormap").getString(errorCode);
		} catch (MissingResourceException ex) {
			errorMessage = "Unknown error.";
		}
		return errorMessage;
	}

}
