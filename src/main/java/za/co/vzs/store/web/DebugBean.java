package za.co.vzs.store.web;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import za.co.vzs.store.domain.Audit;
import za.co.vzs.store.domain.Customer;
import za.co.vzs.store.persistence.AuditResource;
import za.co.vzs.store.service.CustomerService;

@Named("debugBean")
@RequestScoped
public class DebugBean {

	@EJB
	private CustomerService customerService;
	@Inject
	private AuditResource auditResource;

	public DebugBean() {
	}

	public List<Customer> getCustomers() {
		return customerService.getCustomers();
	}
	
	public List<Audit> getAuditRecords(){
		return auditResource.findAll();
	}

}
