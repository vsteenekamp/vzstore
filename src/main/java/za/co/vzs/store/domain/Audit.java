package za.co.vzs.store.domain;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Entity implementation class for Entity: AuditRecord
 *
 */
@Entity
public class Audit implements Serializable {
	
	public enum UpdateType {
		CREATE, UPDATE, DELETE
	}
	
    private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;
	private Calendar date;	
	private int entityId;
	@Enumerated(EnumType.STRING)
	private UpdateType type;
			
	public Audit() {		
	}
		
	public Audit(UpdateType type) {		
		this.type = type;
	}

	public Calendar getDate() {
		return this.date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}   
     
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int customerId) {
		this.entityId = customerId;
	}

	public UpdateType getType() {
		return type;
	}

	public void setType(UpdateType type) {
		this.type = type;
	}		
	   
}
